<?php

namespace Drupal\Tests\entity_recycle\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Contains tests for testing accesses to modules forms and view.
 *
 * @group entity_recycle
 */
class EntityRecycleAccessTest extends BrowserTestBase {

  /**
   * Testing node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user',
    'node',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->createContentType(['type' => 'article']);
    self::$modules[] = 'entity_recycle';
    $this->installModulesFromClassProperty($this->container);

    $this->entity = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
      'recycle_bin' => TRUE,
    ]);
  }

  /**
   * Tests access for modules forms and view.
   *
   * TODO: Fails because of permission issue on entity_recycle.settings route.
   */
  public function testModulePagesAccess() {
    // Settings form.
    $this->assertPageAccess([], TRUE, 403, 'entity_recycle.settings');
    $this->assertPageAccess(['administer entity recycle bin'], TRUE, 200, 'entity_recycle.settings');
    // Entity restore form.
    $this->assertPageAccess([], TRUE, 403, 'entity_recycle.entity.restore', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'id' => $this->entity->id(),
    ]);
    $this->assertPageAccess(['restore entity recycle bin items'], TRUE, 200, 'entity_recycle.entity.restore', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'id' => $this->entity->id(),
    ]);
    // Entity delete form.
    $this->assertPageAccess([], TRUE, 403, 'entity.node.delete_form', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'node' => $this->entity->id(),
    ]);
    $this->assertPageAccess(['add entity recycle bin items'], TRUE, 200, 'entity.node.delete_form', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'node' => $this->entity->id(),
    ]);
    // Node recycle bin view.
    $this->assertPageAccess([], FALSE, 403, '/admin/content/node/recycle-bin');
    $this->assertPageAccess(['view entity recycle bin items'], FALSE, 200, '/admin/content/node/recycle-bin');
  }

  /**
   * User login function.
   *
   * @param array $permissions
   *   User permissions.
   */
  private function loginCreateUser(array $permissions) {
    $this->drupalLogin($this->drupalCreateUser($permissions));
  }

  /**
   * Asserts status code for given page.
   *
   * @param array $permissions
   *   User permissions.
   * @param bool $isRoute
   *   TRUE if $pathName is route name, FALSE if not.
   * @param int $statusCode
   *   Expected status code.
   * @param string $pathName
   *   Name of route or path we want to visit.
   * @param array $parameters
   *   Optional route parameters.
   */
  private function assertPageAccess(array $permissions, bool $isRoute, int $statusCode, string $pathName, array $parameters = []) {
    $this->loginCreateUser($permissions);
    if ($isRoute) {
      $this->drupalGet(Url::fromRoute($pathName, $parameters));
    }
    else {
      $this->drupalGet($pathName);
    }
    $this->assertSession()->statusCodeEquals($statusCode);
  }

}
