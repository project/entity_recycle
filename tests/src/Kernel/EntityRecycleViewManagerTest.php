<?php

namespace Drupal\Tests\entity_recycle\Kernel;

use Drupal\Core\Access\AccessResult;
use Drupal\entity_recycle\EntityRecycleManager;
use Drupal\entity_recycle\EntityRecycleViewManager;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Contains tests for the EntityViewRecycleManager.
 *
 * @group entity_recycle
 */
class EntityRecycleViewManagerTest extends EntityKernelTestBase {
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * EntityRecycleManager service.
   *
   * @var \Drupal\entity_recycle\EntityRecycleViewManager
   */
  protected $entityRecycleViewManager;

  /**
   * Testing node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * Modules configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $entityRecycleConfiguration;

  /**
   * Field storage variable.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $fieldStorage;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'filter',
    'node',
    'entity_recycle',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);

    $this->installConfig(self::$modules);
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'test_1']);
    $this->createContentType(['type' => 'test_2']);

    $this->drupalSetCurrentUser($this->createUser([], [
      'view entity recycle bin items',
      'add entity recycle bin items',
      'restore entity recycle bin items',
      'delete entity recycle bin items',
      'administer entity recycle bin',
    ]));

    $this->entityRecycleConfiguration = $this->config('entity_recycle.settings');
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
      'user' => [],
    ]);
    $this->entityRecycleConfiguration->set('purge_time', 1000)->save();

    $this->fieldStorage = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'field_name' => 'recycle_bin',
        'type' => 'boolean',
        'locked' => TRUE,
        'cardinality' => 1,
        'settings' => [],
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
        'status' => TRUE,
        'translatable' => FALSE,
        'entity_type' => 'node',
      ]);
    $this->fieldStorage->save();

    $this->addFieldToEntity('article');
    $this->entity = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
      'recycle_bin' => TRUE,
    ]);

    $this->entityRecycleViewManager = new EntityRecycleViewManager(
      $this->container->get('current_user'),
      new EntityRecycleManager(
        $this->container->get('config.factory'),
        $this->entityTypeManager,
        $this->container->get('entity_field.manager'),
        $this->container->get('logger.factory'),
        $this->container->get('module_handler')
      )
    );
  }

  /**
   * Tests entityAccess() method.
   */
  public function testEntityAccess() {
    $this->assertEquals(AccessResult::neutral(), $this->entityRecycleViewManager->entityAccess($this->entity));
    $node = $this->createNode([
      'title' => 'Test node 1',
      'uid' => 1,
      'type' => 'test_1',
    ]);
    $this->assertEquals(AccessResult::neutral(), $this->entityRecycleViewManager->entityAccess($node));
    $this->assertEquals(AccessResult::neutral(), $this->entityRecycleViewManager->entityAccess($node, $this->createUser()));

  }

  /**
   * Tests checkViewPermission() method.
   */
  public function testCheckViewPermission() {
    $this->assertTrue($this->entityRecycleViewManager->checkViewPermission());
    $this->assertFalse($this->entityRecycleViewManager->checkViewPermission($this->createUser()));
  }

  /**
   * Tests checkRestorePermission() method.
   */
  public function testCheckRestorePermission() {
    $this->assertTrue($this->entityRecycleViewManager->checkRestorePermission());
    $this->assertFalse($this->entityRecycleViewManager->checkRestorePermission($this->createUser()));
  }

  /**
   * Tests checkDeletePermission() method.
   */
  public function testCheckDeletePermission() {
    $this->assertTrue($this->entityRecycleViewManager->checkDeletePermission());
    $this->assertFalse($this->entityRecycleViewManager->checkDeletePermission($this->createUser()));
  }

  /**
   * Creates and adds recycle_bin field to entity.
   *
   * @param string $bundle
   *   Bundle, to whom method adds field.
   */
  private function addFieldToEntity($bundle) {
    $fieldConfig = [
      'field_storage' => $this->fieldStorage,
      'label' => 'Recycle Bin',
      'settings' => [],
      'bundle' => $bundle,
    ];

    $field = $this->entityTypeManager
      ->getStorage('field_config')
      ->create($fieldConfig);

    $field->save();
  }

}
