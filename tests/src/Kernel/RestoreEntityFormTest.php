<?php

namespace Drupal\Tests\entity_recycle\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\entity_recycle\EntityRecycleManager;
use Drupal\entity_recycle\Form\RestoreEntityForm;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Contains tests for the RestoreEntity form.
 *
 * @group entity_recycle
 */
class RestoreEntityFormTest extends EntityKernelTestBase {
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * Modules configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $entityRecycleConfiguration;

  /**
   * RestoreEntityForm.
   *
   * @var \Drupal\entity_recycle\Form\RestoreEntityForm
   */
  protected $entityRestoreForm;

  /**
   * Testing node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * Field storage variable.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $fieldStorage;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'filter',
    'node',
    'entity_recycle',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);

    $this->installConfig(self::$modules);
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'test_1']);
    $this->createContentType(['type' => 'test_2']);

    $this->entityRecycleConfiguration = $this->config('entity_recycle.settings');
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
      'user' => [],
    ]);
    $this->entityRecycleConfiguration->set('purge_time', 1000)->save();

    $this->fieldStorage = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'field_name' => 'recycle_bin',
        'type' => 'boolean',
        'locked' => TRUE,
        'cardinality' => 1,
        'settings' => [],
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
        'status' => TRUE,
        'translatable' => FALSE,
        'entity_type' => 'node',
      ]);
    $this->fieldStorage->save();

    $this->addFieldToEntity('article');
    $this->entity = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
      'recycle_bin' => TRUE,
    ]);

    $mockRouteMatch = $this->getMockBuilder(RouteMatchInterface::class)->disableOriginalConstructor()->getMock();
    $mockRouteMatch->method('getParameter')->will($this->onConsecutiveCalls('node', $this->entity->id()));
    $this->container->set('current_route_match', $mockRouteMatch);

    $this->entityRestoreForm = new RestoreEntityForm(
      new EntityRecycleManager(
        $this->container->get('config.factory'),
        $this->entityTypeManager,
        $this->container->get('entity_field.manager'),
        $this->container->get('logger.factory'),
        $this->container->get('module_handler')
      ),
      $this->entityTypeManager,
      $this->container->get('module_handler'),
    );
  }

  /**
   * Tests getQuestion() method.
   */
  public function testGetQuestion() {
    $this->assertEquals('Are you sure you want to restore article <em class="placeholder">Test Article</em>?', $this->entityRestoreForm->getQuestion());
  }

  /**
   * Tests getCancelUrl() method.
   */
  public function testCancelUrl() {
    $this->assertEquals(new Url('entity.node.canonical', ['node' => $this->entity->id()]), $this->entityRestoreForm->getCancelUrl());
  }

  /**
   * Tests submitForm() method.
   */
  public function testSubmitForm() {
    $formState = new FormState();
    $form = [];
    $this->assertTrue($this->entity->get('recycle_bin')->value);
    $this->entityRestoreForm->submitForm($form, $formState);
    $node = $this->entityTypeManager->getStorage('node')->load($this->entity->id());
    $this->assertEquals($node->get('recycle_bin')->value, "0");
    $this->assertEquals(new Url('entity.node.canonical', ['node' => $this->entity->id()]), $formState->getRedirect());
  }

  /**
   * Creates and adds recycle_bin field to entity.
   *
   * @param string $bundle
   *   Bundle, to whom method adds field.
   */
  private function addFieldToEntity($bundle) {
    $fieldConfig = [
      'field_storage' => $this->fieldStorage,
      'label' => 'Recycle Bin',
      'settings' => [],
      'bundle' => $bundle,
    ];

    $field = $this->entityTypeManager
      ->getStorage('field_config')
      ->create($fieldConfig);

    $field->save();
  }

}
