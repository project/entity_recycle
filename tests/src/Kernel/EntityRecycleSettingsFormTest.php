<?php

namespace Drupal\Tests\entity_recycle\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\entity_recycle\EntityRecycleManager;
use Drupal\entity_recycle\Form\EntityRecycleSettingsForm;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Contains tests for the EntityRecycleSettings form.
 *
 * @group entity_recycle
 */
class EntityRecycleSettingsFormTest extends EntityKernelTestBase {
  use ContentTypeCreationTrait;

  /**
   * Modules configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $entityRecycleConfiguration;

  /**
   * EntityRecycleSettingsForm.
   *
   * @var \Drupal\entity_recycle\Form\EntityRecycleSettingsForm
   */
  protected $entityRecycleSettingsForm;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'filter',
    'node',
    'entity_recycle',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);

    $this->installConfig(self::$modules);
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'test_1']);
    $this->createContentType(['type' => 'test_2']);

    $this->entityRecycleConfiguration = $this->config('entity_recycle.settings');
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
      'user' => [],
    ]);
    $this->entityRecycleConfiguration->set('purge_time', 1000)->save();

    $this->entityRecycleSettingsForm = new EntityRecycleSettingsForm(
      $this->container->get('config.factory'),
      $this->entityTypeManager,
      new EntityRecycleManager(
        $this->container->get('config.factory'),
        $this->entityTypeManager,
        $this->container->get('entity_field.manager'),
        $this->container->get('logger.factory'),
        $this->container->get('module_handler')
      )
    );
  }

  /**
   * Tests the building of the form.
   */
  public function testBuildForm() {
    $formState = new FormState();
    $form = [];
    $form = $this->entityRecycleSettingsForm->buildForm($form, $formState);
    $this->assertNotNull($form);
    $this->assertCount(0, $formState->getErrors());
    $this->assertArrayHasKey('general', $form);
    $this->assertArrayHasKey('purge_time', $form['general']);
    $this->assertEquals($this->entityRecycleConfiguration->get('purge_time'), $form['general']['purge_time']['#default_value']);
    $this->assertArrayHasKey('entity_types', $form['general']);
  }

  /**
   * Tests the submitting of the form.
   *
   * TODO: Fails because entityType can't be removed once is selected and saved.
   */
  /* public function testFormSubmit() { */
  /*   $formState = new FormState(); */
  /*   $form = []; */
  /*   $form = $this->entityRecycleSettingsForm->buildForm($form, $formState); */
  /*   $formState->setValue('general', [ */
  /*     'node' => [ */
  /*       'bundles' => [ */
  /*         'article' => 'article', */
  /*         'test_1' => 'test_1', */
  /*         'test_2' => 'test_2', */
  /*       ], */
  /*     ], */
  /*     'entity_types' => [ */
  /*       'node' => 'node', */
  /*     ], */
  /*     'purge_time' => 666, */
  /*   ]); */
  /*   $this->assertNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin')); */
  /*   $this->assertNull($this->entityTypeManager->getStorage('field_config')->load('node.test_2.recycle_bin')); */
  /*   $this->assertEquals([ */
  /*     'node' => [ */
  /*       'article' => 'article', */
  /*     ], */
  /*     'user' => [], */
  /*   ], $this->entityRecycleConfiguration->get('types')); */
  /*   $this->assertEquals(1000, $this->entityRecycleConfiguration->get('purge_time')); */
  /*   $this->entityRecycleSettingsForm->submitForm($form, $formState); */
  /*   $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.article.recycle_bin')); */
  /*   $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin')); */
  /*   $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_2.recycle_bin')); */
  /*   $this->assertEquals(666, $this->config('entity_recycle.settings')->get('purge_time')); */
  /*   $this->assertEquals([ */
  /*     'node' => [ */
  /*       'article' => 'article', */
  /*       'test_1' => 'test_1', */
  /*       'test_2' => 'test_2', */
  /*     ], */
  /*   ], $this->config('entity_recycle.settings')->get('types')); */
  /* } */

  /**
   * Tests updateRecycleBinField() method.
   */
  public function testUpdateRecycleBinField() {
    $formState = new FormState();
    $formState->setValue('general', [
      'node' => [
        'bundles' => [
          'article' => 'article',
          'test_1' => 'test_1',
          'test_2' => 'test_2',
        ],
      ],
      'entity_types' => [
        'node' => 'node',
      ],
    ]);
    $this->assertNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin'));
    $this->assertNull($this->entityTypeManager->getStorage('field_config')->load('node.test_2.recycle_bin'));
    $this->entityRecycleSettingsForm->updateRecycleBinField($formState);
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.article.recycle_bin'));
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin'));
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_2.recycle_bin'));
  }

  /**
   * Test getSubmittedTypes() method.
   */
  public function testGetSubmittedTypes() {
    $formState = new FormState();
    $formState->setValue('general', [
      'node' => [
        'bundles' => [
          'article' => 'article',
          'test_1' => 'test_1',
          'test_2' => 'test_2',
        ],
      ],
      'entity_types' => [
        'node' => 'node',
      ],
    ]);
    $this->assertEquals([
      'node' => [
        'article' => 'article',
        'test_1' => 'test_1',
        'test_2' => 'test_2',
      ],
    ], $this->entityRecycleSettingsForm->getSubmittedTypes($formState));
  }

}
