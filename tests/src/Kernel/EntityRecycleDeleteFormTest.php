<?php

namespace Drupal\Tests\entity_recycle\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_recycle\EntityRecycleManager;
use Drupal\entity_recycle\Form\EntityRecycleDeleteForm;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Symfony\Component\Routing\Route;

/**
 * Contains tests for the EntityRecycleDelete form.
 *
 * @group entity_recycle
 */
class EntityRecycleDeleteFormTest extends EntityKernelTestBase {
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * Modules configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $entityRecycleConfiguration;

  /**
   * RestoreEntityForm.
   *
   * @var \Drupal\entity_recycle\Form\RestoreEntityForm
   */
  protected $entityRecycleDeleteForm;

  /**
   * Testing node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * Field storage.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $fieldStorage;

  /**
   * Mocked RouteMatch service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $mockRouteMatch;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'filter',
    'node',
    'entity_recycle',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);

    $this->installConfig(self::$modules);
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'test_1']);
    $this->createContentType(['type' => 'test_2']);

    $this->drupalSetCurrentUser($this->createUser([], [
      'add entity recycle bin items',
    ]));

    $this->entityRecycleConfiguration = $this->config('entity_recycle.settings');
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
      'user' => [],
    ]);
    $this->entityRecycleConfiguration->set('purge_time', 1000)->save();

    $this->fieldStorage = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'field_name' => 'recycle_bin',
        'type' => 'boolean',
        'locked' => TRUE,
        'cardinality' => 1,
        'settings' => [],
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
        'status' => TRUE,
        'translatable' => FALSE,
        'entity_type' => 'node',
      ]);
    $this->fieldStorage->save();

    $this->addFieldToEntity('article');
    $this->entity = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
      'recycle_bin' => TRUE,
    ]);

    $this->mockRouteMatch = $this->getMockBuilder(RouteMatchInterface::class)->disableOriginalConstructor()->getMock();
    $this->container->set('current_route_match', $this->mockRouteMatch);
    $this->entityRecycleDeleteForm = new EntityRecycleDeleteForm(
      $this->container->get('entity.form_builder'),
      new EntityRecycleManager(
        $this->container->get('config.factory'),
        $this->entityTypeManager,
        $this->container->get('entity_field.manager'),
        $this->container->get('logger.factory'),
        $this->container->get('module_handler')
      ),
      $this->container->get('module_handler')
    );
  }

  /**
   * Tests the building of the form.
   */
  public function testBuildForm() {
    $this->mockRouteMatch->method('getParameter')->willReturn($this->entity);
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route('/node/{node}/delete', [], [], ['entity_type' => 'node']));
    $form = [];
    $formState = new FormState();

    // If entity is already in recycle bin.
    $form = $this->entityRecycleDeleteForm->buildForm($form, $formState);
    $this->assertNotEmpty($form);
    $this->assertCount(0, $formState->getErrors());
    $this->assertNotNull($form['#title']);
    $this->assertEquals('Are you sure you want to permanently delete the article <em class="placeholder">Test Article</em>?', $form['#title']);
    $this->assertNotNull($form['actions']['submit']['#value']);
    $this->assertEquals('Delete', $form['actions']['submit']['#value']);

    // If entity is not in recycle bin.
    $this->entity->set('recycle_bin', FALSE)->save();
    $form = $this->entityRecycleDeleteForm->buildForm($form, $formState);
    $this->assertNotEmpty($form);
    $this->assertCount(0, $formState->getErrors());
    $this->assertNotNull($form['#title']);
    $this->assertEquals('Are you sure you want move article <em class="placeholder">Test Article</em> to recycle bin?', $form['#title']);
    $this->assertNotNull($form['actions']['submit']['#value']);
    $this->assertEquals('Move', $form['actions']['submit']['#value']);

    $this->assertInstanceOf(EntityRecycleDeleteForm::class, $form['actions']['submit']['#submit'][0][0]);
  }

  /**
   * Tests the submitting of the form.
   */
  public function testSubmitForm() {
    $this->mockRouteMatch->method('getParameter')->willReturn($this->entity);
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route('/node/{node}/delete', [], [], ['entity_type' => 'node']));
    $form = [];
    $formState = new FormState();

    // If entity is not in recycle bin.
    $this->entity->set('recycle_bin', FALSE)->save();
    $this->assertFalse($this->entity->get('recycle_bin')->value);
    $this->entityRecycleDeleteForm->submitForm($form, $formState);
    $node = $this->entityTypeManager->getStorage('node')->load($this->entity->id());
    $this->assertEquals($node->get('recycle_bin')->value, "1");

    // If entity is in recycle bin.
    $this->entityRecycleDeleteForm->submitForm($form, $formState);
    $this->assertNull($this->entityTypeManager->getStorage('node')->load($this->entity->id()));
  }

  /**
   * Tests getEntityTypeId() method on success.
   */
  public function testGettingEntityTypeIdOnSuccess() {
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route('/node/{node}/delete', [], [], ['entity_type' => 'node']));
    $this->assertEquals('node', $this->entityRecycleDeleteForm->getEntityTypeId());
  }

  /**
   * Tests getEntityTypeId() method on failure.
   *
   * REASON: getRouteObject() return Route object without 'entity_type' option.
   */
  public function testGettingEntityTypeIdOnNullOption() {
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route(''));
    $this->assertNull($this->entityRecycleDeleteForm->getEntityTypeId());
  }

  /**
   * Tests getEntity() method on success.
   */
  public function testGettingEntityOnSuccess() {
    $this->mockRouteMatch->method('getParameter')->willReturn($this->entity);
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route('/node/{node}/delete', [], [], ['entity_type' => 'node']));
    $this->assertEquals($this->entity, $this->entityRecycleDeleteForm->getEntity());
  }

  /**
   * Tests getEntity() method on failure.
   *
   * REASON: getEntityTypeId() doesn't return entityTypeId.
   */
  public function testGettingEntityOnNullEntityType() {
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route(''));
    $this->assertNull($this->entityRecycleDeleteForm->getEntity());
  }

  /**
   * Tests getEntity() method on failure.
   *
   * REASON: getParameter() doesn't return entity.
   */
  public function testGettingEntityOnNullEntity() {
    $this->mockRouteMatch->method('getRouteObject')->willReturn(new Route('/node/{node}/delete', [], [], ['entity_type' => 'node']));
    $this->mockRouteMatch->method('getParameter')->willReturn(NULL);
    $this->assertNull($this->entityRecycleDeleteForm->getEntity());
  }

  /**
   * Creates and adds recycle_bin field to entity.
   *
   * @param string $bundle
   *   Bundle, to whom method adds field.
   */
  private function addFieldToEntity(string $bundle) {
    $fieldConfig = [
      'field_storage' => $this->fieldStorage,
      'label' => 'Recycle Bin',
      'settings' => [],
      'bundle' => $bundle,
    ];

    $field = $this->entityTypeManager
      ->getStorage('field_config')
      ->create($fieldConfig);

    $field->save();
  }

}
