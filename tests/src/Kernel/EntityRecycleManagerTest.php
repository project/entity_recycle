<?php

namespace Drupal\Tests\entity_recycle\Kernel;

use Drupal\entity_recycle\EntityRecycleManager;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Contains tests for the EntityRecycleManager service.
 *
 * @group entity_recycle
 */
class EntityRecycleManagerTest extends EntityKernelTestBase {
  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * EntityRecycleManager service.
   *
   * @var \Drupal\entity_recycle\EntityRecycleManager
   */
  protected $entityRecycleManager;

  /**
   * Testing node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * Modules configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $entityRecycleConfiguration;

  /**
   * Field storage variable.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $fieldStorage;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'filter',
    'node',
    'entity_recycle',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);

    $this->installConfig(self::$modules);
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'test_1']);
    $this->createContentType(['type' => 'test_2']);

    $this->entityRecycleConfiguration = $this->config('entity_recycle.settings');
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
      'user' => [],
    ]);
    $this->entityRecycleConfiguration->set('purge_time', 1000)->save();

    $this->fieldStorage = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'field_name' => 'recycle_bin',
        'type' => 'boolean',
        'locked' => TRUE,
        'cardinality' => 1,
        'settings' => [],
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
        'status' => TRUE,
        'translatable' => FALSE,
        'entity_type' => 'node',
      ]);
    $this->fieldStorage->save();

    $this->addFieldToEntity('article');
    $this->entity = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
      'recycle_bin' => TRUE,
    ]);

    $this->entityRecycleManager = new EntityRecycleManager(
      $this->container->get('config.factory'),
      $this->entityTypeManager,
      $this->container->get('entity_field.manager'),
      $this->container->get('logger.factory'),
      $this->container->get('module_handler')
    );

  }

  /**
   * Tests getSettings() method.
   */
  public function testGetSettings() {
    $this->assertNotNull($this->entityRecycleManager->getSettings());
    $this->assertEquals($this->entityRecycleConfiguration->get('types'), $this->entityRecycleManager->getSettings()->get('types'));
  }

  /**
   * Tests getSetting() method.
   */
  public function testGetSetting() {
    $this->assertNotNull($this->entityRecycleManager->getSetting('types'));
    $this->assertEquals($this->entityRecycleConfiguration->get('types'), $this->entityRecycleManager->getSetting('types'));
    $this->assertEquals($this->entityRecycleConfiguration->get('purge_time'), $this->entityRecycleManager->getSetting('purge_time'));
  }

  /**
   * Tests isEnabled() method.
   */
  public function testEnabledEntity() {
    $this->assertTrue($this->entityRecycleManager->isEnabled($this->entity, 'article'));
    $this->assertFalse($this->entityRecycleManager->isEnabled($this->entity, 'page'));
    $this->assertFalse($this->entityRecycleManager->isEnabled($this->createUser()));
  }

  /**
   * Tests hasBundles() method.
   */
  public function testHasBundles() {
    $this->assertTrue($this->entityRecycleManager->hasBundles('node'));
    $this->assertFalse($this->entityRecycleManager->hasBundles('user'));
  }

  /**
   * Tests getBundles() method.
   */
  public function testGetBundles() {
    $this->assertEquals(NodeType::loadMultiple(), $this->entityRecycleManager->getBundles('node'));
  }

  /**
   * Tests getEnabledBundles() method.
   */
  public function testGetEnabledBundles() {
    $this->assertEquals(['article' => 'article'], $this->entityRecycleManager->getEnabledBundles('node'));
    $this->assertNull($this->entityRecycleManager->getEnabledBundles('user'));
  }

  /**
   * Tests inRecycleBin() method.
   */
  public function testInRecycleBin() {
    $this->assertTrue($this->entityRecycleManager->inRecycleBin($this->entity, 'article'));
    $this->assertFalse($this->entityRecycleManager->inRecycleBin($this->entity, 'test_2'));
    $this->assertFalse($this->entityRecycleManager->inRecycleBin($this->createUser()));
  }

  /**
   * Tests getItem() method.
   */
  public function testGetItem() {
    $this->entity->set('recycle_bin', TRUE)->save();
    $this->assertNotNull($this->entityRecycleManager->getItem('node', $this->entity->id()));
    $this->entity->set('recycle_bin', FALSE)->save();
    $this->assertNull($this->entityRecycleManager->getItem('node', $this->entity->id()));
    $this->assertNull($this->entityRecycleManager->getItem('node', 123456));
  }

  /**
   * Tests getAllItems() method.
   */
  public function testGetAllItems() {
    $this->entityRecycleConfiguration->set('types', [
      'node' => [
        'article' => 'article',
      ],
    ])->save();
    $this->assertNotEmpty($this->entityRecycleManager->getAllItems());
    $this->entityRecycleConfiguration->set('types', [])->save();
    $this->assertEmpty($this->entityRecycleManager->getAllItems());
  }

  /**
   * Tests addItem() method.
   */
  public function testAddItem() {
    // If entity is not already in recycle bin.
    $this->entity->set('recycle_bin', FALSE)->save();
    $result = $this->entityRecycleManager->addItem($this->entity);
    $this->assertNotFalse($result);
    $this->assertEquals($result->get('recycle_bin')->value, "1");

    // If entity is already in recycle bin.
    $this->entity->set('recycle_bin', TRUE)->save();
    $result = $this->entityRecycleManager->addItem($this->entity);
    $this->assertNotFalse($result);
    $this->assertEquals($result->get('recycle_bin')->value, "1");
  }

  /**
   * Tests removeItem() method.
   */
  public function testRemoveItem() {
    // If entity is in recycle bin.
    $this->entity->set('recycle_bin', TRUE)->save();
    $result = $this->entityRecycleManager->removeItem($this->entity);
    $this->assertEquals($result->get('recycle_bin')->value, "0");

    // If entity is not in recycle bin.
    $this->entity->set('recycle_bin', FALSE)->save();
    $result = $this->entityRecycleManager->removeItem($this->entity);
    $this->assertFalse($result);
  }

  /**
   * Tests fieldExists() method.
   */
  public function testFieldExists() {
    $this->assertTrue($this->entityRecycleManager->fieldExists('node', 'article'));
    $this->assertFalse($this->entityRecycleManager->fieldExists('node', 'test_1'));
  }

  /**
   * Tests createField() method.
   */
  public function testCreateField() {
    $this->assertNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin'));
    $this->entityRecycleManager->createField('node', 'test_1');
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin'));

    $this->assertEquals(
      $this->entityTypeManager->getStorage('field_config')->load('node.article.recycle_bin'),
      $this->entityRecycleManager->createField('node', 'article'));
  }

  /**
   * Tests setDefaultFieldValue() method.
   */
  public function testSetDefaultFieldValue() {
    $this->addFieldToEntity('test_1');
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')->load('node.test_1.recycle_bin'));
    $node = $this->createNode([
      'title' => 'Test node 1',
      'uid' => 1,
      'type' => 'test_1',
    ]);
    $this->assertEmpty(batch_get());
    $this->entityRecycleManager->setDefaultFieldValue('node', 'test_1');
    $batch = batch_get();
    $this->assertNotEmpty($batch);
    $this->assertEquals([
      0 => '\Drupal\entity_recycle\EntityRecycleManager::setEntityFieldValue',
      1 => [$this->entityTypeManager->getStorage('node')->load($node->id())],
    ], $batch['sets'][0]['operations'][0]);
  }

  /**
   * Test setEntityFieldValue() method.
   */
  public function testSetEntityFieldValue() {
    $node = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'article',
    ]);
    $this->assertNull($node->get('recycle_bin')->value);
    $this->entityRecycleManager->setEntityFieldValue($node);
    $this->assertEquals($node->get('recycle_bin')->value, "0");
  }

  /**
   * Tests getFieldStorage() method.
   */
  public function testGetFieldStorage() {
    $this->assertEquals($this->fieldStorage, $this->entityRecycleManager->getFieldStorage('node'));
    $this->assertEmpty($this->entityRecycleManager->getFieldStorage('user'));
  }

  /**
   * Tests getField() method.
   */
  public function testGetField() {
    $this->assertEquals($this->entityTypeManager
      ->getStorage('field_config')
      ->load('node.article.recycle_bin'), $this->entityRecycleManager->getField('node', 'article'));
  }

  /**
   * Tests deleteField() method with provided bundle parameter.
   */
  public function testDeleteBundleField() {
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')
      ->load('node.article.recycle_bin'));
    $this->entityRecycleManager->deleteField('node', 'article');
    $this->assertNull($this->entityTypeManager
      ->getStorage('field_config')
      ->load('node.article.recycle_bin'));
  }

  /**
   * Tests deleteField() method without provided bundle parameter.
   */
  public function testDeleteEntityTypeFields() {
    $this->addFieldToEntity('test_1');
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')
      ->load('node.test_1.recycle_bin'));
    $this->addFieldToEntity('test_2');
    $this->assertNotNull($this->entityTypeManager->getStorage('field_config')
      ->load('node.test_2.recycle_bin'));
    $this->entityRecycleManager->deleteField('node');
    $this->assertNull($this->entityTypeManager->getStorage('field_config')
      ->load('node.test_1.recycle_bin'));
    $this->assertNull($this->entityTypeManager->getStorage('field_config')
      ->load('node.test_2.recycle_bin'));
  }

  /**
   * Tests getPurgeTime() method.
   */
  public function testGetPurgeTime() {
    $this->entity->setPublished();
    $this->entity->setChangedTime(time() - 3000);
    $this->entity->set('recycle_bin', FALSE)->save();
    $this->assertEquals($this->entityRecycleConfiguration->get('purge_time') - 50, $this->entityRecycleManager->getPurgeTime($this->entity));

    // If entity bundle is not enabled.
    $node = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'test_1',
    ]);
    $this->assertFalse($this->entityRecycleManager->getPurgeTime($node));

    // If purge time setting is not set.
    $this->entityRecycleConfiguration->clear('purge_time')->save();
    $this->assertFalse($this->entityRecycleManager->getPurgeTime($this->entity));
  }

  /**
   * Tests purge() method.
   */
  public function testPurge() {
    $node = $this->createNode([
      'title' => 'Test Article 2',
      'uid' => 2,
      'type' => 'article',
      'recycle_bin' => FALSE,
    ]);
    $node->setPublished();
    $node->setChangedTime(time() - 90000)->save();
    $nodeId = $node->id();
    $this->assertTrue($this->entityRecycleManager->purge($node));
    $this->assertNull($this->entityTypeManager->getStorage('node')->load($nodeId));

    // If entity type is not enabled.
    $node = $this->createNode([
      'title' => 'Test Article',
      'uid' => 1,
      'type' => 'test_1',
    ]);
    $this->assertFalse($this->entityRecycleManager->purge($node));

    // If purge time is not set.
    $this->entityRecycleConfiguration->clear('purge_time')->save();
    $this->assertFalse($this->entityRecycleManager->purge($this->entity));
  }

  /**
   * Creates and adds recycle_bin field to entity.
   *
   * @param string $bundle
   *   Bundle, to whom method adds field.
   */
  private function addFieldToEntity(string $bundle) {
    $fieldConfig = [
      'field_storage' => $this->fieldStorage,
      'label' => 'Recycle Bin',
      'settings' => [],
      'bundle' => $bundle,
    ];

    $field = $this->entityTypeManager
      ->getStorage('field_config')
      ->create($fieldConfig);

    $field->save();
  }

}
