<?php

/**
 * @file
 * Hooks provided by the Entity recycle module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Triggers before entity is pushed to recycle bin.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity which will be moved to recycle bin.
 *
 * @see \Drupal\entity_recycle\EntityRecycleManager
 */
function hook_recycle_bin_pre_recycle(EntityInterface &$entity) {
}

/**
 * Triggers after entity is pushed to recycle bin.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity which was moved to recycle bin.
 *
 * @see \Drupal\entity_recycle\EntityRecycleManager
 */
function hook_recycle_bin_recycled(EntityInterface &$entity) {
}

/**
 * Triggers when entity is about to be restored from recycle bin.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity which will be restored.
 *
 * @see \Drupal\entity_recycle\EntityRecycleManager
 */
function hook_recycle_bin_entity_pre_restore(EntityInterface &$entity) {
}

/**
 * Triggers when entity is restored from recycle bin.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Restored entity.
 *
 * @see \Drupal\entity_recycle\Form\RestoreEntityForm
 */
function hook_recycle_bin_entity_restored(EntityInterface &$entity) {
}

/**
 * Triggers before the entity is deleted from recycle bin.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Entity which will be deleted.
 *
 * @see \Drupal\entity_recycle\EntityRecycleManager
 * @see \Drupal\entity_recycle\Form\EntityRecycleDeleteForm
 */
function hook_recycle_bin_entity_pre_delete(EntityInterface &$entity) {
}

/**
 * Triggers when entity is deleted from recycle bin.
 *
 * @see \Drupal\entity_recycle\EntityRecycleManager
 * @see \Drupal\entity_recycle\Form\EntityRecycleDeleteForm
 */
function hook_recycle_bin_entity_deleted() {
}
