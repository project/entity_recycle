<?php

namespace Drupal\entity_recycle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\entity_recycle\EntityRecycleManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Recycled entity alert block.
 *
 * @Block(
 *   id = "recycled_entity_alert_block",
 *   admin_label = @Translation("Recycled entity alert block")
 * )
 */
class RecycledEntityAlertBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * CurrentRoute service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRoute;

  /**
   * Configuration of module.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $modulesConfiguration;

  /**
   * EntityRecycleManager service.
   *
   * @var \Drupal\entity_recycle\EntityRecycleManagerInterface
   */
  protected $entityRecycleManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $routeMatch, ConfigFactoryInterface $configFactory, EntityRecycleManagerInterface $entityRecycleManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRoute = $routeMatch;
    $this->modulesConfiguration = $configFactory->getEditable('entity_recycle.settings');
    $this->entityRecycleManager = $entityRecycleManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('entity_recycle.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [
      '#theme' => 'block_recycled_entity_alert_default',
    ];
    $enabledTypes = $this->modulesConfiguration->get('types');
    foreach ($enabledTypes as $type => $bundles) {
      $entity = $this->currentRoute->getParameter($type);
      if ($entity !== NULL) {
        $entityBundle = $entity->bundle();
        if ($this->entityRecycleManager->inRecycleBin($entity, $entityBundle)) {
          $output['#message'] = $this->t("This item is in the recycle bin,
                so it's only seen by users with a correct permission.");
          $purgeTime = $this->entityRecycleManager->getPurgeTime($entity);
          if ($purgeTime) {
            if ($purgeTime < 0) {
              $output['#purgeMessage'] =
                $this->t('This item will be permanently deleted on the next cron run.');
            }
            else {
              $output['#purgeMessage'] =
                $this->t('This item will be permanently deleted in @time minutes.', [
                  '@time' => $purgeTime,
                ]);
            }
          }
        }
      }
    }
    return $output;
  }

  /**
   * Overriding method to set cache to 0.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
